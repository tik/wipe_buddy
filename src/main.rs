extern crate ansi_term;
#[macro_use] extern crate lazy_static;
extern crate threadpool;
extern crate rand;

use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::{File};
use std::fmt::Display;
use std::sync::{Arc, Mutex};
use std::time::{Instant, Duration};

use ansi_term::Style;
use ansi_term::Colour::*;

use rand::Rng;

lazy_static! {
    static ref BOLD: Style = Style::new().bold();
    static ref UNDERLINE: Style = Style::new().underline();
    static ref HEADING: Style = Yellow.bold();
}

const WRITE_BUFFER_SIZE: usize = 4 * 1024;

fn main() {
    let pool = threadpool::ThreadPool::new(2);

    pool.execute(try_max_brightness);

    let model = get_file_contents("/sys/devices/virtual/dmi/id/product_version");
    println!("\n\n    {} running on {}\n", BOLD.paint("wipe buddy"), BOLD.paint(model));
    
    let disks = parse_disks();
    
    show_disk_status(&disks);
    loop {
        println!("\nPress {} to exit, {} for a list of available commands:", BOLD.paint("q"), BOLD.paint("h"));
        
        let input = read_one_line_from_stdin();
        match input.trim() {
            "q" => return,
            "h" => print_help(),
            "l" => show_disk_status(&disks),
            "w" => select_disk_to_wipe(&disks),
            "s" => try_shutdown(&disks),
            ""  => show_disk_status(&disks),
            com =>  println!("unknown command: {}", com),
        }
        
        update_jobs(&disks, &pool);
    }
}


fn show_disk_status(disks: &Vec<Arc<DiskInformation>>) {
    println!("{}", HEADING.paint("Status of found disks:") );

    let longest = disks.iter().fold(0, |b, e| std::cmp::max(b, e.name.len())) +1;

    for disk in disks {
        println!("{:6}) {}:{:width$} {} -- {}",
            disk.id,
            BOLD.paint(&*disk.name),
            " ",
            disk.size,
            *disk.state.lock().unwrap(),
            width = longest - disk.name.len()
        );
    }
}

fn parse_disks() -> Vec<Arc<DiskInformation>> {
    let partitions = get_file_contents("/proc/partitions");
    let disks = parse_partition_file(&partitions);

    let mounts = get_command_output("mount");
    
    mark_mounted_disks(&disks, mounts);

    disks
}

fn parse_partition_file(partitions: &String) -> Vec<Arc<DiskInformation>> {
    let lines = partitions.split("\n").skip(2);
    
    let mut disks = Vec::new();
    let mut i = 0;
    for line in lines {
        let line = line.trim();
        //println!("line: {:?}", line);
        
        let mut parts = line.split_whitespace();
        let (_major, minor, no_blocks, name) = (parts.next(), parts.next(), parts.next(), parts.next());
        
        if let Some(name) = name {
            if  minor.map_or(false, |input| input.parse::<u64>().map(|n| n % 16 == 0).unwrap_or(false))
                    && 
                (name.starts_with("sd") || name.starts_with("nvme") || name.starts_with("xvd") || name.starts_with("mmcblk")) {
                /// Contains the major and minor numbers of  each  partition  as  well  as  the  number  of 1024-byte blocks and the partition name.
                let no_blocks: u64 = no_blocks.unwrap_or("0").parse().unwrap_or(0);
                i += 1;
                disks.push(Arc::new(DiskInformation {
                    id: i,
                    name: name.into(),
                    size: KiloByte(no_blocks),
                    state: Mutex::new(Detected), // TODO erkenne wann eine Disk nicht überschrieben werden darf
                }));
            }
        }
    }
    
    disks
}

fn mark_mounted_disks(disks: &Vec<Arc<DiskInformation>>, mounts: String) {
    for disk in disks {
        let mut mount_lines = mounts.split('\n').filter(|e: &&str| {
            e.starts_with("/dev/")
        });
        
        let disk_path = format!("/dev/{}", disk.name);
        
        if mount_lines.any(|e| e.starts_with(&*disk_path)) {
            *disk.state.lock().unwrap() = Protected;
        }
    }
}

fn get_file_contents(path: &str) -> String {
    let f = match File::open(path) {
        Ok(f) => f,
        Err(_) => return format!("<unable to open {:?}", path),
    };
    let reader = BufReader::new(f);
    let mut buffer = String::new();

    let mut i = 0;
    for line in reader.lines() {
        i += 1;
        buffer += &* line.unwrap_or( format!("<read error on line {} - {}>\n", i, path) );
        buffer += "\n";
    }
    
    buffer
}

fn replace_file_contents(path: &str, contents: &String) -> io::Result<()> {
    let mut file = File::create(path)?;
    
    file.write_all(contents.as_bytes())
}

fn get_command_output<S: Into<String>>(command: S) -> String {
    let command = command.into();
    let mut iter = command.split_whitespace();
    let programm = iter.next().expect("No binary supplied");
    let args: Vec<&str> = iter.collect();
    let output = std::process::Command::new(&programm)
        .args(&args)
        .output()
        .expect(&*format!("unable to execute: {:?}", command));

    String::from_utf8_lossy(&output.stdout).to_string()
}

fn read_one_line_from_stdin() -> String {
    print!("> ");
    io::stdout().flush().expect("Unable to flush stdout inside fn read_one_line_from_stdin()");
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap_or(0);
    input
}

/// Shutdown system if everything is done or errored
fn try_shutdown(disks: &Vec<Arc<DiskInformation>>) {
    if disks.iter().all(|e| {
        match *e.state.lock().unwrap() {
            Detected | Protected | Wiped{ took:_ } | WipeFailed => true,
            QueueForWipe | Wiping{ progress: _, per_second: _ } => false,
        }
    }) {
        get_command_output("shutdown -h now");
    } else {
        println!("unable to shutdown, some action is pending");
        show_disk_status(&disks);
    }
}

#[derive(Debug)]
struct DiskInformation {
    id: u64,
    size: ByteUnit,
    name: String,
    state: Mutex<DiskState>,
}

impl std::cmp::PartialEq<Self> for DiskInformation {
    fn eq(&self, rhs: &Self) -> bool {
        self.id == rhs.id &&
        self.size == rhs.size &&
        self.name == rhs.name &&
        *self.state.lock().unwrap() == *rhs.state.lock().unwrap()
    }
}

#[derive(Debug,PartialEq,Eq)]
enum DiskState {
    Detected,
    Protected,
    QueueForWipe,
    Wiping{ progress: ByteUnit, per_second: ByteUnit },
    Wiped{ took: Duration },
    WipeFailed,
}
use DiskState::*;

impl Display for DiskState {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            &Detected => write!(f, "Detected"),
            &Protected => write!(f, "Protected"),
            &QueueForWipe => write!(f, "Queued for Wipe"),
            &Wiping { ref progress, ref per_second } => write!(f, "Wiping {} ({}/s)", progress, per_second),
            &Wiped { ref took } => write!(f, "Wiped successfully in {} seconds", took.as_secs()),
            &WipeFailed=> write!(f, "Wipe failed"),
        }
    }
}


#[derive(Debug,Eq,Ord,Clone)]
enum ByteUnit {
    Byte(u64),
    KiloByte(u64),
}
use ByteUnit::*;

impl ByteUnit {
    pub fn to_byte(&self) -> ByteUnit {
        match self {
            &Byte(v) => Byte(v),
            &KiloByte(v) => Byte(1024 * v),
        }
    }
    
    pub fn byte_per_second(&self, periode: Duration) -> ByteUnit {
        match self {
            &Byte(v) => {
                let secs = periode.as_secs();
                
                Byte(if secs == 0 {
                    0
                } else {
                    v / secs
                })
            },
            something_else => something_else.to_byte().byte_per_second(periode),
        }
    }
}

impl std::cmp::PartialEq<Self> for ByteUnit {
    fn eq(&self, rhs: &Self) -> bool {
        match self {
            &Byte(l) => match rhs {
                &Byte(r) => l == r,
                &KiloByte(r) => l == 1024 * r,
            },
            &KiloByte(l) => match rhs {
                &Byte(r) => 1024 * l == r,
                &KiloByte(r) => l == r,
            }
        }
    }
}
impl std::cmp::PartialOrd<Self> for ByteUnit {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(match self {
            &Byte(l) => match other {
                &Byte(r) => l.cmp(&r),
                ref something_else => {
                    if let Byte(r) = something_else.to_byte() {
                        l.cmp(&r)
                    } else {
                        panic!("fn partial_cmp<ByteUnit>: this must never happen!");
                    }
                },
            },
            ref something_else => something_else.to_byte().cmp(other),
        })
    }
}
impl std::ops::Add for ByteUnit {
    type Output = ByteUnit;

    fn add(self, other: Self) -> Self {
        match self {
            Byte(l) => match other {
                Byte(r) => Byte(l + r),
                something_else => self + something_else.to_byte(),
            },
            something_else => something_else.to_byte() + other,
        }
    }
}
impl std::ops::AddAssign for ByteUnit {
    fn add_assign(&mut self, other: Self) {
        match self {
            &mut Byte(ref mut me) => match other {
                Byte(r) => { *me += r; return },
                KiloByte(r) => { *me += r * 1024; return; },
            },
            &mut KiloByte(ref mut me) => match other {
                Byte(_r) => { /* *me += r / 1024; würde zu einem Präzisions führen */ },
                KiloByte(r) => { *me += r; return },
            },
        }

        // Change Type of self
        let mut v = self.to_byte() + other;
        std::mem::swap(self, &mut v);
    }
}
impl std::ops::Sub for ByteUnit {
    type Output = ByteUnit;

    fn sub(self, other: Self) -> Self {
        match self {
            Byte(l) => match other {
                Byte(r) => Byte(l - r),
                something_else => self - something_else.to_byte(),
            },
            something_else => something_else.to_byte() - other,
        }
    }
}

impl Display for ByteUnit {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            &Byte(size) => {
                if size < 1000 {
                    return write!(f, "{:>6.2} B ", size);
                }
                
                return KiloByte(size / 1024).fmt(f);
            }
            &KiloByte(size) => {   
                if size < 1000 {
                    return write!(f, "{:>6.2} KB", size);
                }
                
                let size = size as f64;                
                let size: f64 = size / 1024.0;
                if size < 1000.0 {
                    return write!(f, "{:>6.2} MB", size);
                }
                
                let size: f64 = size / 1024.0;
                if size < 1000.0 {
                    return write!(f, "{:>6.2} GB", size);
                }
                
                let size: f64 = size / 1024.0;
                write!(f, "{:>6.2} TB", size)
            }
        }
    }
}


fn print_help() {
    println!("\n{}", HEADING.paint("Available Commands:"));
    
    let options = [
        ("q", "quit"),
        ("h", "print this help"),
        ("l", "list found disks and status"),
        ("w", "start wiping a disk"),
        ("s", "shutdown this system"),
    ];
    
    for option in options.iter() {
        println!("       {}        {}", BOLD.paint(option.0), option.1);
    }
}

fn select_disk_to_wipe(disks: &Vec<Arc<DiskInformation>>) {
    println!("{}", HEADING.paint("Enter the id of the disk you would like to be wiped:"));
    
    match read_one_line_from_stdin().trim().parse() {
        Ok(id) => {
            if let Some(disk) = disks.iter().find(|ref e| e.id == id && *e.state.lock().unwrap() == Detected) {
                while {
                    println!("\nWiping is {}!\nDestroy any data on {} ({})\nEnter {} to continue or {} to abort?", UNDERLINE.paint("non-reversable"), BOLD.paint(&*disk.name), disk.size, BOLD.paint("y"), BOLD.paint("a"));
                    
                    match read_one_line_from_stdin().trim() {
                        "y" | "Y" | "j" | "J" => { false },
                        "a" => { return }
                        _ => { println!("{}", Red.paint("invalid response")); true }
                    }
                    
                } {}
                
                *disk.state.lock().unwrap() = QueueForWipe;
                
                return;
            }
        },
        Err(e) => println!("parse_error: {:?}", e),
    }
    
    println!("{}", Red.paint("invalid selection"));
}

fn update_jobs(disks: &Vec<Arc<DiskInformation>>, pool: &threadpool::ThreadPool) {
    for disk in disks {
        if *disk.state.lock().unwrap() == QueueForWipe {
            println!("Queued {} for wipe", BOLD.paint(&*disk.name));
            let disk = disk.clone();
            
            pool.execute(move || {
                println!("{}{}", Green.paint("Wiping "), BOLD.paint(&*disk.name));

                match wipe_disk(&disk) {
                    Ok(duration) => {
                        *disk.state.lock().unwrap() = Wiped{ took: duration };
                        println!("{}{} in {} seconds", 
                            Green.dimmed().paint("Wiped "), 
                            BOLD.paint(&*disk.name),
                            duration.as_secs()
                        );
                    },
                    Err(e) => {
                        *disk.state.lock().unwrap() = WipeFailed;
                        println!("{}{}{}",
                            Red.paint("Wipe of "),
                            BOLD.paint(&*disk.name),
                            Red.paint(format!(" failed: {:?}", e))
                        );
                        return
                    }
                }

            })
        }
    }
}

fn wipe_disk(disk: &Arc<DiskInformation>) -> std::io::Result<std::time::Duration> {
    let mut rng = rand::thread_rng();

    *disk.state.lock().unwrap() = Wiping{ progress: Byte(0), per_second: Byte(0) };

    let mut buf = Box::new( [0; WRITE_BUFFER_SIZE] );
    let mut position = Byte(0);
    let mut disk_file = std::fs::OpenOptions::new()
                        .write(true)
                        .open(&*format!("/dev/{}", disk.name))?;

    let mut i: u64 = 0;
    let now = Instant::now();
    while position < disk.size {
        rng.fill_bytes(&mut *buf);
        disk_file.write_all(&*buf)?;

        position += Byte(WRITE_BUFFER_SIZE as u64);
        
        i += 1;
        if i % 1024*1024 == 0 {
            *disk.state.lock().unwrap() = Wiping{ 
                progress: position.clone(), 
                per_second: position.byte_per_second(now.elapsed()),
            };
        }
    }

    Ok( now.elapsed() )
}


fn try_max_brightness() {
    let max = get_file_contents("/sys/class/backlight/intel_backlight/max_brightness");
    
    replace_file_contents("/sys/class/backlight/intel_backlight/brightness", &max).expect("unable to set max brightness");
}


#[cfg(test)]
mod test {
    use ::*;
    
    #[test]
    fn unit_eq() {
        let one_k_bytes = Byte(1024);
        let one_kilobyte = KiloByte(1);
        
        assert_eq!(one_k_bytes, one_kilobyte);
        assert_eq!(one_kilobyte, one_k_bytes);
    }
    
    #[test]
    fn format_byte() {
        assert_eq!("    42 B ", format!("{}", Byte(42)));
    }
    #[test]
    fn format_kilobyte() {
        assert_eq!("    42 KB", format!("{}", KiloByte(42)));
    }
    #[test]
    fn format_megabyte() {
        assert_eq!(" 42.00 MB", format!("{}", KiloByte(42 * 1024)));
    }
    #[test]
    fn format_gigabyte() {
        assert_eq!(" 42.00 GB", format!("{}", KiloByte(42 * 1024 * 1024)));
    }
    #[test]
    fn format_terrabyte() {
        assert_eq!(" 42.00 TB", format!("{}", KiloByte(42 * 1024 * 1024 * 1024)));
    }


    #[test]
    fn ord_eqals() {
        let one_k_bytes = Byte(1024);
        let one_kilobyte = KiloByte(1);

        assert!(one_k_bytes <= one_kilobyte);
        assert!(one_kilobyte <= one_k_bytes);

        assert!(one_k_bytes >= one_kilobyte);
        assert!(one_kilobyte >= one_k_bytes);
    }
    #[test]
    fn ord_less() {
        let two_k_bytes = Byte(2048);
        let one_kilobyte = KiloByte(1);

        assert!(two_k_bytes > one_kilobyte);
        assert!(one_kilobyte < two_k_bytes);
    }
    #[test]
    fn ord_more() {
        let one_k_bytes = Byte(1024);
        let two_kilobytes = KiloByte(2);

        assert!(two_kilobytes > one_k_bytes);
        assert!(one_k_bytes < two_kilobytes);
    }


    #[test]
    fn add_bytes() {
        assert_eq!(Byte(4), Byte(2) + Byte(2));
    }
    #[test]
    fn add_kilobytes() {
        assert_eq!(KiloByte(4), KiloByte(2) + KiloByte(2));
    }
    #[test]
    fn add_bytes_mixed_up() {
        assert_eq!(Byte(2 + 2048), Byte(2) + KiloByte(2));
    }
    #[test]
    fn add_bytes_mixed_down() {
        assert_eq!(Byte(2 + 2048), KiloByte(2) + Byte(2));
    }

    #[test]
    fn add_assign_bytes() {
        let mut four = Byte(2);
        four += Byte(2);
        assert_eq!(Byte(4), four);
    }
    #[test]
    fn add_assign_kilobytes() {
        let mut four = KiloByte(2);
        four += KiloByte(2);
        assert_eq!(KiloByte(4), four);
    }
    #[test]
    fn add_assign_mixed_up() {
        let mut four = Byte(2);
        four += KiloByte(2);
        assert_eq!(Byte(2 + 2048), four);
    }
    #[test]
    fn add_assign_mixed_down() {
        let mut four = KiloByte(2);
        four += Byte(2);
        assert_eq!(Byte(2 + 2048), four);
    }
    
    
    #[test]
    fn sub_bytes() {
        assert_eq!(Byte(0), Byte(2) - Byte(2));
    }
    #[test]
    fn sub_kilobytes() {
        assert_eq!(KiloByte(0), KiloByte(2) - KiloByte(2));
    }
    #[test]
    fn sub_bytes_mixed_up() {
        assert_eq!(Byte(2), Byte(2048 + 2) - KiloByte(2));
    }
    #[test]
    fn sub_bytes_mixed_down() {
        assert_eq!(Byte(2048 - 2), KiloByte(2) - Byte(2));
    }

    #[test]
    fn delta_byte_per_second_0() {
        let zero = Byte(0);
        let no_time = ::std::time::Duration::new(0, 0);

        let zero_bps = zero.byte_per_second( no_time );
        assert_eq!(zero, zero_bps)
    }

    #[test]
    fn parse_partition_file_sda_mmcblk0() {
        let parts = "
        major minor  #blocks  name

        8        0  500107608 sda
        8        1     524288 sda1
        8        2  499582279 sda2
        179        0   31166976 mmcblk0
        179        1   31162880 mmcblk0p1
        254        0  499580231 dm-0
        254        1   20971520 dm-1
        254        2  478605312 dm-2
        ".into();
        let partitions = parse_partition_file(&parts);

        assert_eq!(2, partitions.len());

        let sda = DiskInformation {
            id: 1,
            name: "sda".into(),
            size: KiloByte(500107608),
            state: Mutex::new(Detected),
        };
        assert_eq!(sda, *partitions[0]);

        let mmcblk0 = DiskInformation {
            id: 2,
            name: "mmcblk0".into(),
            size: KiloByte(31166976),
            state: Mutex::new(Detected),
        };
        assert_eq!(mmcblk0, *partitions[1]);
    }

    #[test]
    fn protect_sda() {
        let disks = vec![ Arc::new(DiskInformation {
            id: 0,
            name: "sda".into(),
            size: Byte(0),
            state: Mutex::new(Detected),
        }) ];

        let mounts = "/dev/sda3 on / type ext4 (rw,noatime,nodiratime,errors=remount-ro,data=ordered)".into();

        mark_mounted_disks(&disks, mounts);

        assert_eq!(Protected, *(*disks[0]).state.lock().unwrap());
    }

    #[test]
    fn protect_sdb() {
        let disks = vec![ 
            Arc::new(DiskInformation {
                id: 1,
                name: "sda".into(),
                size: Byte(0),
                state: Mutex::new(Detected),
            }),
            Arc::new(DiskInformation {
                id: 2,
                name: "sdb".into(),
                size: Byte(0),
                state: Mutex::new(Detected),
            }),
        ];

        let mounts = r#"
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
sys on /sys type sysfs (rw,nosuid,nodev,noexec,relatime)
dev on /dev type devtmpfs (rw,nosuid,relatime,size=8049952k,nr_inodes=2012488,mode=755)
run on /run type tmpfs (rw,nosuid,nodev,relatime,mode=755)
/dev/sdb3 on / type btrfs (rw,relatime,space_cache,subvolid=5,subvol=/)
securityfs on /sys/kernel/security type securityfs (rw,nosuid,nodev,noexec,relatime)
tmpfs on /dev/shm type tmpfs (rw,nosuid,nodev)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=000)
tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,mode=755)
cgroup on /sys/fs/cgroup/systemd type cgroup (rw,nosuid,nodev,noexec,relatime,xattr,release_agent=/usr/lib/systemd/systemd-cgroups-agent,name=systemd)
pstore on /sys/fs/pstore type pstore (rw,nosuid,nodev,noexec,relatime)
cgroup on /sys/fs/cgroup/blkio type cgroup (rw,nosuid,nodev,noexec,relatime,blkio)
cgroup on /sys/fs/cgroup/freezer type cgroup (rw,nosuid,nodev,noexec,relatime,freezer)
cgroup on /sys/fs/cgroup/cpu,cpuacct type cgroup (rw,nosuid,nodev,noexec,relatime,cpu,cpuacct)
cgroup on /sys/fs/cgroup/pids type cgroup (rw,nosuid,nodev,noexec,relatime,pids)
cgroup on /sys/fs/cgroup/net_cls type cgroup (rw,nosuid,nodev,noexec,relatime,net_cls)
cgroup on /sys/fs/cgroup/cpuset type cgroup (rw,nosuid,nodev,noexec,relatime,cpuset)
cgroup on /sys/fs/cgroup/perf_event type cgroup (rw,nosuid,nodev,noexec,relatime,perf_event)
cgroup on /sys/fs/cgroup/memory type cgroup (rw,nosuid,nodev,noexec,relatime,memory)
cgroup on /sys/fs/cgroup/devices type cgroup (rw,nosuid,nodev,noexec,relatime,devices)
systemd-1 on /proc/sys/fs/binfmt_misc type autofs (rw,relatime,fd=36,pgrp=1,timeout=0,minproto=5,maxproto=5,direct)
debugfs on /sys/kernel/debug type debugfs (rw,relatime)
configfs on /sys/kernel/config type configfs (rw,relatime)
mqueue on /dev/mqueue type mqueue (rw,relatime)
hugetlbfs on /dev/hugepages type hugetlbfs (rw,relatime)
tmpfs on /tmp type tmpfs (rw,nosuid,nodev)
/dev/sdb4 on /boot type ext2 (rw,relatime,block_validity,barrier,user_xattr,acl)
tmpfs on /run/user/0 type tmpfs (rw,nosuid,nodev,relatime,size=1612120k,mode=700)
fusectl on /sys/fs/fuse/connections type fusectl (rw,relatime)
        "#.into();

        mark_mounted_disks(&disks, mounts);

        assert_eq!(Detected, *(*disks[0]).state.lock().unwrap());
        assert_eq!(Protected, *(*disks[1]).state.lock().unwrap());
    }
}
